# Form radio

Form radios and form radio groups for general use inside forms.

Note: Form radio groups using subcomponents are not implemented yet as there are some weird side effects of wrapping individual radio buttons.
