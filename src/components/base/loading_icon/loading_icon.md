## Under the hood
Loading icon uses pure css to render a spinner.

## Additional notes
> The previous version of this loading icon made use of the size property with a numeric value between `1` and `5`. Going forward, the size options are being deprecated and the use of `sm`, `md`, `lg`, and `xl` are required.

> For future reference, here's the previous size mapping used:
> * size of 1 (which was 14px) is converted to sm (which is 16px)
> * size of 2 or 3 (which was 28px or 42px) is converted to lg (which is 32px)
> * size of 4 or 5 (which was 56px or 70px) is to converted to xl (which is 64px)
