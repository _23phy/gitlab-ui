import description from './new_dropdown.md';
import examples from './examples';

export default {
  description,
  bootstrapComponent: 'b-dropdown',
  examples,
  followsDesignSystem: true,
};
